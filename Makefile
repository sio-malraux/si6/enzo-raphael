SOURCE = src/main.cpp src/affichage.cpp src/affichage.h src/calculatoire.cpp src/calculatoire.h

OBJETS = obj/main.o obj/affichage.o obj/calculatoire.o

PROGRAM = bin/programme-shared

STATIC = libpg_affichage.a


$(PROGRAM): obj/main.o $(STATIC)
	g++ -g -o $@ obj/main.o -L. $(STATIC) -lpq


obj/main.o: src/main.cpp
	g++ -I/usr/include/postgresql -fPIC -Wall -g -c -o $@ $^


obj/affichage.o: src/affichage.cpp
	g++ -I/usr/include/postgresql -fPIC -Wall -g -c -o $@ $^


obj/calculatoire.o: src/calculatoire.cpp
	g++ -I/usr/include/postgresql -fPIC -Wall -g -c -o $@ $^


$(STATIC): obj/affichage.o obj/calculatoire.o
	ar rcs $@ $^


libpg_affichage.so: obj/affichage.o obj/calculatoire.o
	g++ -shared -Wl,-soname,libpg_affichage.so.1 -o libpg_affichage.so.0.0 $^ -lpq
	ln -sf libpg_affichage.so.0 libpg_affichage.so


.PHONY: objets programme clean


objets: $(OBJETS)


programme:$(PROGRAM)


clean:
	rm -r $(OBJETS) $(PROGRAM) $(STATIC)

