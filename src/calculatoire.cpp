#include <libpq-fe.h>
#include <cstring>
#include "affichage.h"
#include "calculatoire.h"

int calculLongEntete(PGresult * resultat)
{
	//savoir quel est l'entête avec le plus de caractères
	int colonnes = PQnfields(resultat);
	char * entete;
	int charEntete;
	int longEntete=0;

	for(int i=0 ; i<colonnes ; i++)
	{
		entete = PQfname(resultat, i);
		charEntete = strlen(entete);

		if(longEntete < charEntete)
		{
			longEntete = charEntete;
		}
	}

	return longEntete;
}

