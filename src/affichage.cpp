#include <iostream>
#include <libpq-fe.h>
#include <cstring>
#include "affichage.h"
#include "calculatoire.h"

void infoConnexion(PGconn * codeConnexion)
{
	//affichage des informations de connexion

	char * hote;
	char * utilisateur;
	char * motdepasse;
	char * nomDB;
	char * port;
	int chiffrementSSL;
	const char * encodage;
	int verProto;
	int verServ;
	int verLibpq;

	//Renvoie le nom d'hôte du serveur utilisé pour la connexion.
	hote = PQhost(codeConnexion);

	//Renvoie le nom d'utilisateur utilisé pour la connexion.
	utilisateur = PQuser(codeConnexion);

	//Renvoie le mot de passe utilisé pour la connexion.
	motdepasse = PQpass(codeConnexion);

	//Renvoie le nom de la base de données de la connexion.
	nomDB = PQdb(codeConnexion);

	//Renvoie le numéro de port utilisé pour la connexion.
	port = PQport(codeConnexion);

	//Retourne la structure SSL utilisée dans la connexion ou NULL si SSL n'est pas utilisé.
	chiffrementSSL = PQsslInUse(codeConnexion);

	//Recherche un paramétrage actuel du serveur. (encodage)
	encodage = PQparameterStatus(codeConnexion, "server_encoding");

	//Interroge le protocole interface/moteur lors de son utilisation.
	verProto = PQprotocolVersion(codeConnexion);

	//Renvoie un entier représentant la version du moteur.
	verServ = PQserverVersion(codeConnexion);

	//Renvoie la version de libpq™ en cours d'utilisation.
	verLibpq = PQlibVersion();


	//affichage

	std::cout << "La connexion au serveur de base de données " << hote << " a été établie avec les paramètres suivants :" << std::endl;
	std::cout << "\t* utilisateur : " << utilisateur << std::endl;
	std::cout << "\t* mot de passe : ";
	for(long unsigned int i=0 ; i<strlen(motdepasse) ; i++)
	{
		std::cout << "*";
	}
	std::cout << " ( <-- autant de ’*’ que la longueur du mot de passe)" << std::endl;
	std::cout << "\t* base de donnée : " << nomDB << std::endl;
	std::cout << "\t* port TCP : " << port << std::endl;
	std::cout << "\t* chiffrement SSL : ";
	if(chiffrementSSL == 1)
	{
		std::cout << "true" << std::endl;
	}
	else
	{
		std::cout << "false" << std::endl;
	}
	std::cout << "\t* encodage : " << encodage << std::endl;
	std::cout << "\t* version du protocole : " << verProto << std::endl;
	std::cout << "\t* version du serveur : " << verServ << std::endl;
	std::cout << "\t* version de la bibliothèque ’libpq’ du client : "<< verLibpq << std::endl;

	std::cout << std::endl;
}




void infoRequete(PGresult * resultat)
{
	//affichage des informations de l'état de la requête
	ExecStatusType requeteStatus;
	requeteStatus = PQresultStatus(resultat);

//PQresultStatus peut renvoyer une des valeurs suivantes :

	//PGRES_EMPTY_QUERY
	//La chaîne envoyée au serveur était vide.
	if(requeteStatus == PGRES_EMPTY_QUERY)//0
	{
		std::cout << "\tLa chaîne envoyée au serveur était vide." << std::endl;
	}

	//PGRES_COMMAND_OK
	//Fin avec succès d'une commande ne renvoyant aucune donnée.
	else if(requeteStatus == PGRES_COMMAND_OK)//1
	{
		std::cout << "\tSuccès de la requête ne renvoyant aucune donnée." << std::endl;
	}

	//PGRES_TUPLES_OK
	//Fin avec succès d'une commande renvoyant des données (telle que SELECT ou SHOW).
	else if(requeteStatus == PGRES_TUPLES_OK)//2
	{
		std::cout << "\tSuccès de la requête renvoyant des données." << std::endl;
	}

	//PGRES_COPY_OUT
	//Début de l'envoi (à partir du serveur) d'un flux de données.
	else if(requeteStatus == PGRES_COPY_OUT)//3
	{
		std::cout << "\tDébut de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
	}

	//PGRES_COPY_IN
	//Début de la réception (sur le serveur) d'un flux de données.
	else if(requeteStatus == PGRES_COPY_IN)//4
	{
		std::cout << "\tDébut de la réception (sur le serveur) d'un flux de données." << std::endl;
	}

	//PGRES_BAD_RESPONSE
	//La réponse du serveur n'a pas été comprise.
	else if(requeteStatus == PGRES_BAD_RESPONSE)//5
	{
		std::cout << "\tLa réponse du serveur n'a pas été comprise." << std::endl;
	}

	//PGRES_NONFATAL_ERROR
	//Une erreur non fatale (une note ou un avertissement) est survenue.
	else if(requeteStatus == PGRES_NONFATAL_ERROR)//6
	{
		std::cout << "\tUne erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
	}

	//PGRES_FATAL_ERROR
	//Une erreur fatale est survenue.
	else if(requeteStatus == PGRES_FATAL_ERROR)//7
	{
		std::cout << "\tUne erreur fatale est survenue." << std::endl;
	}

	//PGRES_COPY_BOTH
	//Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, so this status should not occur in ordinary applications.
	else if(requeteStatus == PGRES_COPY_BOTH)//8
	{
		std::cout << "\tLancement du transfert de données Copy In/Out (vers et à partir du serveur)." << std::endl;
	}

	//PGRES_SINGLE_TUPLE
	//La structure PGresult contient une seule ligne de résultat provenant de la commande courante.
	else if(requeteStatus == PGRES_SINGLE_TUPLE)//9
	{
		std::cout << "\tLa structure PGresult contient une seule ligne de résultat provenant de la commande courante." << std::endl;
	}
}




void affichageResultat(PGresult * resultat, const char * sep)
{
	//affichage du résultat de la requête
	int lignes = PQntuples(resultat);

	//affichage du résultat de la requête sql
	tracerLigne(resultat);//affichage de la ligne de séparation avant le tableau

	affichageEntete(resultat, sep);//affichage de l'ENTÊTE

	tracerLigne(resultat);//affichage de la ligne de séparation entre l'entête et les lignes

	for(int i=0 ; i<lignes ; i++)//affichage des LIGNES
	{
		affichageLigne(resultat, sep, i);
	}

	tracerLigne(resultat);//affichage de la ligne de séparation à la fin du tableau

	std::cout << std::endl << "L'exécution de la requête SQL a retourné " << lignes << " enregistrements." << std::endl;

}




void tracerLigne(PGresult * resultat)
{
	//affichage de la ligne de séparation
	int colonnes = PQnfields(resultat);
	int longEntete = calculLongEntete(resultat);
	for(int i=0 ; i<colonnes ; i++)//pour chaque colonne
	{
		//le séparateur vaut deux espaces et un "|" = 3 caractères
		std::cout << "---";
		for(int s=0 ; s<longEntete ; s++)//autant de traits que la longueur de l'entête, car tous les entêtes auront la même longueur, avec les espaces
		{
			std::cout << "-";
		}
	}
	std::cout << "--" << std::endl;//dernier séparateur, avec un trait de moins, car c'est un espace
}




void ajoutEspace(int longEntete, int Char)
{
	//espaces à ajouter pour égaliser
	for(int s=0 ; s<(longEntete - Char) ; s++)
	{
		std::cout << " ";
	}
}




void affichageEntete(PGresult * resultat, const char * sep)
{
	//affichage de l'ENTÊTE
	int colonnes = PQnfields(resultat);
	char * entete;
	int charEntete;
	int longEntete = calculLongEntete(resultat);

	for(int i=0 ; i<colonnes ; i++)
	{
		std::cout << sep;//séparateur

		entete = PQfname(resultat, i);
		charEntete = strlen(entete);
		std::cout << entete;

		ajoutEspace(longEntete, charEntete);//espaces à ajouter pour égaliser
	}
	std::cout << sep << std::endl;
}




void affichageLigne(PGresult * resultat, const char * sep, int i)
{
	//affichage d'une ligne (i)
	int colonnes = PQnfields(resultat);
	char * valeurCase;
	int charLigne;
	int longEntete = calculLongEntete(resultat);

	for(int j=0 ; j<colonnes ; j++)
	{
		std::cout << sep;

		//char* PQgetvalue(const PGresult *res, int row_number, int column_number);
		valeurCase = PQgetvalue(resultat, i, j);//contenu de la case
		charLigne = PQgetlength(resultat, i, j);//longueur de la case actuelle, en caractères

		if(charLigne > longEntete)//cette case est plus grande que l'entête
		{
			reductionLigne(valeurCase, longEntete);//affichage réduit de la case
		}
		else
		{
			std::cout << valeurCase;//affichage complet de la case

			ajoutEspace(longEntete, charLigne);//espaces à ajouter pour égaliser
		}
	}
	std::cout << sep << std::endl;
}




void reductionLigne(char * valeurCase, int longEntete)
{
	//affichage réduit de la case
	for(int c=0 ; c<(longEntete-3) ; c++)
	{
		std::cout << valeurCase[c];
	}
	std::cout << "...";
}

