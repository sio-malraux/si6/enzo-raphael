#ifndef _AFFICHAGE_H
#define _AFFICHAGE_H

#include <iostream>
#include <libpq-fe.h>
#include <cstring>
#endif

void infoConnexion(PGconn *);
void infoRequete(PGresult *);
void affichageResultat(PGresult *, const char *);
void tracerLigne(PGresult *);
void ajoutEspace(int, int);
void affichageEntete(PGresult *, const char *);
void affichageLigne(PGresult *, const char *, int);
void reductionLigne(char *, int);
