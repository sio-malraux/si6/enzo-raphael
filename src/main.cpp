#include <iostream>
#include <libpq-fe.h>
#include <cstring>
#include "affichage.h"
#include "calculatoire.h"

/*
commande pour compiler le fichier main.cpp en exécutable :
g++ $(pkg-config --cflags libpq) -o bin/main src/main.cpp -lpq
*/

int main()
{
	//déclaration des variables
	PGPing codeRetourPing;
	PGconn * codeConnexion;
	//SELECT "Animal".id as "Id", "Animal".nom as "Nom de l'animal", "Animal".sexe as "Sexe", "Animal".date_naissance as "Date de naissance", "Animal".commentaires as "Commentaires", "Race".nom as "Race", "Race".description as "Description" FROM si6."Animal" INNER JOIN si6."Race" ON "Animal".race_id = "Race".id WHERE "Animal".sexe = 'Femelle' AND "Race".nom = 'Singapura';
	const char * requete = "SELECT \"Animal\".id as \"Id\", \"Animal\".nom as \"Nom de l'animal\", \"Animal\".sexe as \"Sexe\", \"Animal\".date_naissance as \"Date de naissance\", \"Animal\".commentaires as \"Commentaires\", \"Race\".nom as \"Race\", \"Race\".description as \"Description\" FROM si6.\"Animal\" INNER JOIN si6.\"Race\" ON \"Animal\".race_id = \"Race\".id WHERE \"Animal\".sexe = 'Femelle' AND \"Race\".nom = 'Singapura';";
	PGresult * resultat;
	const char * sep = " | "; //séparateur


	codeRetourPing = PQping("host=postgresql.bts-malraux72.net port=5432");
	//renvoie l'état du serveur
	if(codeRetourPing == PQPING_OK)
	{
		std::cout << "La connexion au serveur de base de données a été établie avec succès" << std::endl;

		//question 3 :
		//établit une nouvelle connexion à un serveur de bases de données.
		codeConnexion = PQconnectdb("host=postgresql.bts-malraux72.net port=5432");

		infoConnexion(codeConnexion);//affichage des informations de connexion

		//question 4 :
		resultat = PQexec(codeConnexion, requete);

		//question 5 :
		infoRequete(resultat);//affichage des informations de l'état de la requête

		//question 6 :
		affichageResultat(resultat, sep);

		PQclear(resultat);
	}
	else
	{
		std::cerr << "Malheureusement le serveur n’est pas joignable. Vérifier la connectivité" << std::endl;
	}

	return 0;
}

